
import React, { useState, useEffect } from "react";
import './App.css';
import { Button, Jumbotron, Form, FormGroup, Label, Input, Container, Row, Col } from 'reactstrap';

//const spacetimeDataUrl = "http://undefined.com/spacetime.json";

const spacetimeDataUrl = "https://immense-anchorage-79576.herokuapp.com/http://undefined.com/spacetime.json";

function App() {

  const [episodes, setEpisodes] = useState({});
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    getSpacetimeData();
  }, []);

  const getSpacetimeData = async () => {
    console.log("Fetching: "+spacetimeDataUrl);
    const response = await fetch(spacetimeDataUrl);
    const jsonData = await response.json();
    setEpisodes(jsonData);
  };



  const filterEpisodes = (episodes, query) => {
    if (!query) {
        return [];
    }

    return episodes.filter((ep) => {
        const body = ep.body.toLowerCase();
        return body.includes(query.toLowerCase());
    });
  };  

  
  const filteredEpisodes = filterEpisodes(episodes, searchQuery);






  return (
    <div className="App">
      <Jumbotron>
        <h1 className="display-3">PBS SpaceTime Episode Grokker</h1>
        <p className="lead">Enter keywords below, and I will find PBS SpaceTime YouTube episodes that include the keyword</p>
      </Jumbotron>
      <Container>

          <Row>
            <Col sm="2"></Col>
            <Col sm="1">
              <p className="lead">Keywords:</p>
            </Col>
            <Col sm="6">
              <Form>
                    <Label htmlFor="keyword" className="visually-hidden">Enter Keywords from episodes</Label>
                    <Input type="keyword" name="keyword" id="keyword" placeholder="Matt O'Dowd" value={searchQuery} onChange={(e) => setSearchQuery(e.target.value)}/>
              </Form>
            </Col>
          </Row>

          {filteredEpisodes.map( episode => (
            <Row>
              <Col sm="2"></Col>
              <Col style={{textAlign: "left"}} sm="6">{episode.name}</Col>
              <Col sm="1"><a href={episode.url} target="_blank">Watch</a></Col>
              <Col sm="2">{episode.date}</Col>
            </Row>
          ))}

      </Container>
      <footer className="footer">
        <Row>
          <Col sm="2" className="text-muted">Episode Count: {episodes && episodes.length}</Col>
          <Col sm="2">Last Update: 2021-05-22 6:55 EDT</Col>
          <Col></Col>
        </Row>
      </footer>
    </div>
  );
}

export default App;
